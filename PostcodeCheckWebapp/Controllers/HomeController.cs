﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PostcodeCheck.DatabaseContext;
using PostcodeCheck.DomainModel;
using PostcodeCheck.FileSystem;
using PostcodeCheck.SniffRunPlanStrategies;
using PostcodeCheckWebapp.Models;
using PostcodeCheckWebapp.Models.HomeViewModels;
using PostcodeCheckWebapp.Services;

namespace PostcodeCheckWebapp.Controllers
{
    public class HomeController : Controller
    {
        private readonly PostcodeCheckerContext _context;
        private readonly SocketManager _socketManager;

        public HomeController(PostcodeCheckerContext context, SocketManager socketManager)
        {
            _context = context;
            _socketManager = socketManager;
        }

        public IActionResult Index()
        {
            return View(_context.Scans.ToList());
        }

        public IActionResult NewCsvScan()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> NewCsvScan(NewCsvScanViewModel model)
        {
            if (ModelState.IsValid)
            {
                var addressList = AddressListCSV.getAdressListFromCSV(model.ZipcodeCsv.OpenReadStream());

                startScan(model.ScanName, model.ScanStrategy, addressList);

                return RedirectToAction("TestLiveFeed");
                //return View("TestAddressOverview", data);
            }

            return View();
        }

        private async Task startScan(string name, string strategy, List<Address> addressList)
        {

            ProxyRunPlan plan = new ProxyRunPlan(name, strategy, addressList);
            //plan.runAsyncWithProxy(0, 0);

            while (!allDone(plan.RunJobs))
            {
                _socketManager.SendMessageToAllAsync(JsonConvert.SerializeObject(plan));
                Thread.Sleep(500);
            }

            _socketManager.SendMessageToAllAsync(JsonConvert.SerializeObject(plan));

        }

        //todo: verplaatsen naar hoger level
        private static bool allDone(List<RunJob> jobs)
        {
            int count = 0;
            foreach (var run in jobs)
            {
                if (run.job.IsCompleted)
                    count++;
            }

            return count == jobs.Count;
        }

        public IActionResult TestMap()
        {
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult TestLiveFeed()
        {
            return View();
        }
    }
}
