﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PostcodeCheckWebapp.Data.Migrations
{
    public partial class initialmigration1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_AspNetUserRoles_UserId",
                table: "AspNetUserRoles");

            migrationBuilder.DropIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles");

            migrationBuilder.EnsureSchema(
                name: "account");

            migrationBuilder.RenameTable(
                name: "AspNetUsers",
                newSchema: "account");

            migrationBuilder.RenameTable(
                name: "AspNetUserTokens",
                newSchema: "account");

            migrationBuilder.RenameTable(
                name: "AspNetUserRoles",
                newSchema: "account");

            migrationBuilder.RenameTable(
                name: "AspNetUserLogins",
                newSchema: "account");

            migrationBuilder.RenameTable(
                name: "AspNetUserClaims",
                newSchema: "account");

            migrationBuilder.RenameTable(
                name: "AspNetRoles",
                newSchema: "account");

            migrationBuilder.RenameTable(
                name: "AspNetRoleClaims",
                newSchema: "account");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                schema: "account",
                table: "AspNetUserClaims",
                nullable: false,
                oldClrType: typeof(int))
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                schema: "account",
                table: "AspNetRoleClaims",
                nullable: false,
                oldClrType: typeof(int))
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                schema: "account",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                schema: "account",
                table: "AspNetUserTokens",
                column: "UserId",
                principalSchema: "account",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                schema: "account",
                table: "AspNetUserTokens");

            migrationBuilder.DropIndex(
                name: "RoleNameIndex",
                schema: "account",
                table: "AspNetRoles");

            migrationBuilder.RenameTable(
                name: "AspNetUsers",
                schema: "account");

            migrationBuilder.RenameTable(
                name: "AspNetUserTokens",
                schema: "account");

            migrationBuilder.RenameTable(
                name: "AspNetUserRoles",
                schema: "account");

            migrationBuilder.RenameTable(
                name: "AspNetUserLogins",
                schema: "account");

            migrationBuilder.RenameTable(
                name: "AspNetUserClaims",
                schema: "account");

            migrationBuilder.RenameTable(
                name: "AspNetRoles",
                schema: "account");

            migrationBuilder.RenameTable(
                name: "AspNetRoleClaims",
                schema: "account");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "AspNetUserClaims",
                nullable: false,
                oldClrType: typeof(int))
                .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "AspNetRoleClaims",
                nullable: false,
                oldClrType: typeof(int))
                .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_UserId",
                table: "AspNetUserRoles",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName");
        }
    }
}
