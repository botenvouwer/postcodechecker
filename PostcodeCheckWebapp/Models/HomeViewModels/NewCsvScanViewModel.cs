﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PostcodeCheckWebapp.Models.HomeViewModels
{
    public class NewCsvScanViewModel
    {
        [Required]
        public IFormFile ZipcodeCsv { get; set; }

        [Required]
        public string ScanName { get; set; }

        [Required]
        public string ScanStrategy { get; set; }

    }
}
