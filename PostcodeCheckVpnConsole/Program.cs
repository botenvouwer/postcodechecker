﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using PostcodeCheck.DatabaseContext;
using PostcodeCheck.SniffRunPlanStrategies;
using System.Threading;

namespace PostcodeCheckVpnConsole
{
    public class Program
    {
        
        private static IConfigurationRoot configuration;
        private static VpnRunPlan runPlan;
        private static Stopwatch stopWatch;
        
        static async Task Main(string[] args)
        {
            //string scanName = "validatie_viberq";
            string scanName = "viber_q_short_scan_2021_10";
            //string scanName = "viber_q_2021_10";
            string scanStrategy = "viberq";
            
            stopWatch = new Stopwatch();
            stopWatch.Start();
            
            Console.WriteLine("Iniatiating sniff");
            
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("config.json");

            configuration = builder.Build();
            
            PostcodeCheckerContextFactory.connectionString = configuration.GetSection("connectionString").Value;
            
            runPlan = new VpnRunPlan(scanName, scanStrategy);
            
            var isNotScannedCriteria = configuration.GetSection("isNotScannedCriteria").GetChildren().Select(n => int.Parse(n.Value)).ToArray();
            var isNotAnalyzedCriteria = configuration.GetSection("isNotAnalyzedCriteria").GetChildren().Select(n => int.Parse(n.Value)).ToArray();
            
            runPlan.sniffer.isNotScannedCriteria = isNotScannedCriteria;
            runPlan.sniffer.isNotAnalyzedCriteria = isNotAnalyzedCriteria;
            
            runPlan.runInitialization();
            
            while (!runPlan.job.IsCompleted)
            {
                Console.WriteLine("Waiting init...");
                Thread.Sleep(1000);
            }

            runPlan.runAsync().Wait();

            stopWatch.Stop();
            
            TimeSpan elapsed = stopWatch.Elapsed;
            string str2 = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", elapsed.Hours, elapsed.Minutes, elapsed.Seconds, (elapsed.Milliseconds / 10));
            Console.WriteLine("Ran time: \t\t" + str2);

            // DateTime start = DateTime.Now;
            //
            // await SwitchIpWithVpn();
            // await SwitchIpWithVpn();
            // await SwitchIpWithVpn();
            // await SwitchIpWithVpn();
            // await SwitchIpWithVpn();
            //
            // TimeSpan duration = DateTime.Now - start;
            //
            // Console.WriteLine("Ran for: "+duration);
            // foreach (var i in _instances)
            // {
            //     Console.WriteLine("Wan Ip: " + i.getIpAddress());
            // }

        }

    }
}