﻿using Microsoft.Extensions.Configuration;
using PostcodeCheck.DatabaseContext;
using PostcodeCheck.SniffRunPlanStrategies;
using PostcodeCheck.SniffRunPlanStrategies.core;
using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace PostcodeCheckConsole
{
    class Program
    {
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private static bool Handler(CtrlType sig)
        {
            switch (sig)
            {
                case CtrlType.CTRL_C_EVENT:
                case CtrlType.CTRL_LOGOFF_EVENT:
                case CtrlType.CTRL_SHUTDOWN_EVENT:
                case CtrlType.CTRL_CLOSE_EVENT:
                case CtrlType.CTRL_BREAK_EVENT:
                default:
                    //Sla de gegevens op in database voordat we het porgramma sluiten
                    runPlan.save();
                    return false;
            }
        }

        private static IConfigurationRoot configuration;
        private static RunPlanCore runPlan;
        private static Stopwatch stopWatch;

        static void Main(string[] args)
        {

#if DEBUG
            go(args);
#else
            try
            {
                go(args);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.Read();
            }
#endif
        }

        public static void go(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("config.json");

            configuration = builder.Build();

            //configureer de connectionstring
            PostcodeCheckerContextFactory.connectionString = configuration.GetSection("connectionString").Value;

            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);

            RunBetaRunPlan(args);
            //RunOldRunPlan(args[0], args[1]);

            Console.WriteLine("This is the end!");
            // Console.Read();
        }

        public static void RunBetaRunPlan(string[] args)
        {
            string scanName = args[0];
            string scanStrategy = args[1];
        
            //Eventhandler voor als programma door gebruiker wordt onderbroken
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);

            stopWatch = new Stopwatch();

            stopWatch.Start();
            Console.WriteLine("Iniatiating sniff");

            if (args.Length > 2)
            {
                // runPlan = new ProxyRunPlan(scanName, scanStrategy);
                runPlan = new LocalRunPlan(scanName, scanStrategy);
            }
            else
            {
                runPlan = new LocalRunPlan(scanName, scanStrategy);
            }

            var isNotScannedCriteria = configuration.GetSection("isNotScannedCriteria").GetChildren().Select(n => int.Parse(n.Value)).ToArray();
            var isNotAnalyzedCriteria = configuration.GetSection("isNotAnalyzedCriteria").GetChildren().Select(n => int.Parse(n.Value)).ToArray();
            
            runPlan.runInitialization();

            runPlan.sniffer.isNotScannedCriteria = isNotScannedCriteria;
            runPlan.sniffer.isNotAnalyzedCriteria = isNotAnalyzedCriteria;

            while (!runPlan.job.IsCompleted)
            {
                printInitialization();
                Thread.Sleep(300);
            }

            //799 scans per keer
            //121 minuten wachten per 799 scans
            //runPlan.runAsyncWithProxy(799, 121);

            if (args.Length > 2)
            {
                int queueSize = Int32.Parse(args[2]);
                int queueDelay = Int32.Parse(args[3]);

                // ProxyRunPlan rp = (ProxyRunPlan) runPlan;
                // rp.runAsyncWithAllProxies(queueSize, queueDelay);
                
                LocalRunPlan rp = (LocalRunPlan) runPlan;
                rp.runAsync(queueSize, queueDelay);
            }
            else
            {
                LocalRunPlan rp = (LocalRunPlan) runPlan;
                rp.runAsync();
            }

            while (!runPlan.allDone)
            {
                print();
                Thread.Sleep(300);
            }
            stopWatch.Stop();
            print();
        }

        public static void printInitialization()
        {
            Console.Clear();
            Console.WriteLine("Busy sniffing: \t\t" + runPlan.scan);
            Console.WriteLine("Status: \t\t" + runPlan.Status);
            TimeSpan elapsed = stopWatch.Elapsed;
            string str2 = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", elapsed.Hours, elapsed.Minutes, elapsed.Seconds, (elapsed.Milliseconds / 10));
            Console.WriteLine("Running time: \t\t" + str2);

            if (runPlan.Status == JobStatusInitiation.BindingAddresses)
            {
                Console.WriteLine("Binding address: \t\t" + runPlan.AddressBindCount);
            }
        }

        public static void print()
        {
            Console.Clear();
            Console.WriteLine("Busy sniffing: \t\t" + runPlan.scan);
            Console.WriteLine("Status: \t\t" + runPlan.Status);
            TimeSpan elapsed = stopWatch.Elapsed;
            string str2 = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", elapsed.Hours, elapsed.Minutes, elapsed.Seconds, (elapsed.Milliseconds / 10));
            Console.WriteLine("Running time: \t\t" + str2);
            Console.WriteLine("Number of Jobs: \t\t" + runPlan.RunJobs.Count);

            Console.WriteLine("");

            int jobsDone = 0;
            foreach (var runJob in runPlan.RunJobs)
            {
                jobsDone += runJob.currentJob;

                if (runJob.usedProxy != null)
                {
                    Console.WriteLine("Proxy: \t\t\t" + runJob.usedProxy.ip);
                }

                Console.WriteLine("Job status: \t\t" + runJob.JobStatus);
                Console.WriteLine("Total jobs: \t\t" + runJob.totalJobs);
                Console.WriteLine("");
                Console.WriteLine("Running job " + runJob.currentJob + " of " + runJob.totalJobs);
                Console.WriteLine("");
                Console.WriteLine("Last subject: " + runJob.lastSubject);
                Console.WriteLine("");
            }

            Console.WriteLine("Total Scan Results: \t\t" + runPlan.totalScanResults);
            Console.WriteLine("Total Scan Jobs: \t\t" + runPlan.totalNeedScanJobs);
            Console.WriteLine("Total Analyze Jobs: \t\t" + runPlan.totalNeedAnalyseJobs);
            Console.WriteLine("Total Jobs: \t\t" + (runPlan.totalNeedAnalyseJobs + runPlan.totalNeedScanJobs));
            Console.WriteLine("Jobsdone: \t\t" + jobsDone);
        }

        public static void RunOldRunPlan()
        {
            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();
            Console.WriteLine("Iniatiating sniff");

            OldRunPlan runPlan = new OldRunPlan("RekamAlles", "rekam");

            Task run1 = new Task(runPlan.run);
            run1.Start();

            while (!run1.IsCompleted)
            {
                Console.Clear();
                Console.WriteLine("Busy sniffing: " + runPlan.scan);
                Console.WriteLine("");
                Console.WriteLine("Running job " + runPlan.currentJob + " of " + runPlan.totalScanResults);
                Console.WriteLine("");
                Console.WriteLine("Last subject: " + runPlan.lastSubject);
                Console.WriteLine("");

                TimeSpan elapsed = stopWatch.Elapsed;
                string str2 = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", elapsed.Hours, elapsed.Minutes, elapsed.Seconds, (elapsed.Milliseconds / 10));
                Console.WriteLine("Running time: " + str2);
                Thread.Sleep(60);
            }

            stopWatch.Stop();

            Console.Clear();
            Console.WriteLine("Busy sniffing: " + runPlan.scan);
            Console.WriteLine("");
            Console.WriteLine("Running job " + runPlan.currentJob + " of " + runPlan.totalScanResults);
            Console.WriteLine("");
            Console.WriteLine("Last subject: " + runPlan.lastSubject);
            Console.WriteLine("");

            TimeSpan elapsed2 = stopWatch.Elapsed;
            string str3 = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", elapsed2.Hours, elapsed2.Minutes, elapsed2.Seconds, (elapsed2.Milliseconds / 10));
            Console.WriteLine("Running time: " + str3);
            Console.WriteLine("");
            Console.WriteLine("Done, see database for results");
            Console.Read();
        }
    }
}
