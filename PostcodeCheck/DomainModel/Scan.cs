﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PostcodeCheck.DomainModel
{
    [Table("scan")]
    public class Scan
    {
        [Key]
        [Column("pk_scan")]
        public int ID { get; set; }

        [Column("scan_name")]
        [Required]
        [MaxLength(200)]
        public string scanName { get; set; }

        [Column("scan_date")]
        [Required]
        public DateTime scanDate { get; set; }

        [Column("scan_strategy")]
        [Required]
        [MaxLength(200)]
        public string scanStrategy { get; set; }

        public override string ToString()
        {
            return string.Format("Scan {0} -> {1}:{3} {2}", ID, scanName, scanDate, scanStrategy);
        }
    }
}
