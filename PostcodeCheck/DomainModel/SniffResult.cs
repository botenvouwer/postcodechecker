﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PostcodeCheck.DomainModel
{
    [Table("scan_result")]
    public class SniffResult
    {
        public SniffResult()
        {

        }

        public SniffResult(Address address)
        {
            this.address = address;
        }

        [Key]
        [Column("pk_scan_result")]
        public int ID { get; set; }

        [Column("fk_scan")]
        public int scanID { get; set; }

        [Column("scan")]
        [ForeignKey("scanID")]
        public Scan scan { get; set; }

        [Column("external_key")]
        [MaxLength(20)]
        public string externalKey { get; set; }

        [Column("fk_address")]
        public int addressID { get; set; }

        //[Column("address")]
        [ForeignKey("addressID")]
        public Address address { get; set; }

        [Column("raw_result")]
        public string rawResult { get; set; }

        [Column("code")]
        public int code { get; set; } = -5;

        [Column("scan_date")]
        public DateTime scanDate { get; set; }

        [Column("fk_proxy")]
        public int? proxyID { get; set; }

        //[Column("used_proxy")]
        [ForeignKey("proxyID")]
        public ProxyData usedProxy { get; set; }

        [Column("message")]
        [MaxLength(600)]
        public string message { get; set; } = "";

        [Column("availability")]
        public bool availability { get; set; }

        [Column("plan_code")]
        public int planCode { get; set; }

        [Column("planning")]
        [MaxLength(200)]
        public string planning { get; set; }

        [Column("coper_speed")]
        public int coperSpeed { get; set; }

        [Column("fiber_speed")]
        public int fiberSpeed { get; set; }

        [Column("upgrade_speed")]
        public int upgradeSpeed { get; set; }

        public string format(SniffResult sniffResult)
        {
            if (sniffResult == null)
                return "id;zipcode;housenumber;addressAddition;availability;coperspeed;fiberspeed;upgradespeed;upgradeplanning;code;message\r\n";

            return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10}\r\n", (object)sniffResult.address.provId, (object)sniffResult.address.zipcode, (object)sniffResult.address.houseNumber, (object)sniffResult.address.addressAddition, (object)sniffResult.availability, (object)sniffResult.coperSpeed, (object)sniffResult.fiberSpeed, (object)sniffResult.upgradeSpeed, (object)sniffResult.planning, (object)sniffResult.code, (object)sniffResult.message);
        }

        public override string ToString()
        {
            return string.Format("availability={0}, code={1}, message={2}", availability, code, message);
        }
    }
}
