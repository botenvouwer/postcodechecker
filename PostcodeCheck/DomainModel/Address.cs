﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PostcodeCheck.DomainModel
{
    [Table("address")]
    public class Address
    {
        [Key]
        [Column("pk_address")]
        public int ID { get; set; }
        
        [Column("external_key")]
        [MaxLength(10)]
        public string provId { get; set; }

        [Column("zipcode")]
        [Required]
        [MaxLength(6)]
        public string zipcode { get; set; }
        
        [Column("house_number")]
        [Required]
        public int houseNumber { get; set; }
        
        [Column("address_addition")]
        [MaxLength(10)]
        public string addressAddition { get; set; }

        public string ToAddresString()
        {
            return string.Format("{0} {1}{2}", this.provId, zipcode, houseNumber, addressAddition);
        }

        public override string ToString()
        {
            return string.Format("{0}:\t{1}\t{2}{3}", this.provId, zipcode, houseNumber, addressAddition);
        }
    }
}
