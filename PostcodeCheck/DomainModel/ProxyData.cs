﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;

namespace PostcodeCheck.DomainModel
{
    [Table("proxy")]
    public class ProxyData
    {
        [Key]
        [Column("pk_proxy")]
        public int ID { get; set; }
        
        [Column("ip")]
        [Required]
        [MaxLength(45)]
        public string ip { get; set; }

        [Column("port")]
        public int port { get; set; }

        [Column("user")]
        [MaxLength(200)]
        public string user { get; set; }
        
        [Column("pass")]
        [MaxLength(200)]
        public string pass { get; set; }

        [Column("in_use")]
        public bool inUse { get; set; }

        public override bool Equals(object obj)
        {
            var data = obj as ProxyData;
            return data != null &&
                   ID == data.ID;
        }

        public WebProxy getProxy()
        {
            NetworkCredential cr = new NetworkCredential(user, pass);
            WebProxy proxy = new WebProxy(ip + ":" + port) { BypassProxyOnLocal = false, Credentials = cr };
            return proxy;
        }



    }
}
