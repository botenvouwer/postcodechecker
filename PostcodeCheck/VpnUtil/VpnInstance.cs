using System;

namespace PostcodeCheck.VpnUtil
{
    public class VpnInstance
    {
        private string ipAddress;
        public DateTime? _start;
        public DateTime? _end;

        public VpnInstance(string ipAddress)
        {
            this.ipAddress = ipAddress;
        }

        public int cooldownInMinutes { get; set; } = 16;

        public string getIpAddress()
        {
            return ipAddress;
        }
        
        public TimeSpan getDuration()
        {
            TimeSpan? elapsed = _start - _end;
            return (TimeSpan) elapsed;
        }

        public void start()
        {
            _start = DateTime.Now;
            _end = null;
        }

        public void end()
        {
            _end = DateTime.Now;
        }

        public bool isCooledDown()
        {
            if (!_end.HasValue)
            {
                return false;
            }

            TimeSpan? elapsed = DateTime.Now - _end;

            return (elapsed.Value.TotalMinutes > cooldownInMinutes);
        }


    }
}