using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PostcodeCheck.VpnUtil
{
    public class VpnUtilSwitcher
    {
         private static List<VpnInstance> _instances = new List<VpnInstance>();
        
        private static string _vpnPath = @"C:\Program Files\OpenVPN Connect";
        private static string _vpnPathExe = @"C:\Program Files\OpenVPN Connect\OpenVPNConnect.exe";

        static void GetVpnProcess()
        {
            Process[] pname = Process.GetProcessesByName("OpenVPNConnect");

            //Process[] pname = Process.GetProcesses();
            
            Console.WriteLine(pname.Length);

            foreach (var p in pname)
            {
                Console.WriteLine(p.ProcessName);
            }
        }

        static bool IsVpnRunning()
        {
            Process[] pname = Process.GetProcessesByName("OpenVPNConnect");

            if (pname.Length >= 4)
            {
                return true;
            }

            return false;
        }
        
        static bool IsVpnOff()
        {
            Process[] pname = Process.GetProcessesByName("OpenVPNConnect");

            if (pname.Length > 0)
            {
                return false;
            }

            return true;
        }

        static async Task WaitAndCloseVpn()
        {
            Console.WriteLine("Closing VPN");
            
            QuitVpn();

            while (!IsVpnOff())
            {
                await Task.Delay(300);
            }
            
            await Task.Delay(8000);
        }

        public static async Task<VpnInstance> SwitchIpWithVpn()
        {
            Console.WriteLine("VPN Switch init");
            while (true)
            {
                int tryCount = 1;

                while (5 >= tryCount)
                {
                    if (!IsVpnOff())
                    {
                        await WaitAndCloseVpn();
                    }

                    string wanIp = await WaitAndStartVpn();
                    VpnInstance instance = GetInstance(wanIp);

                    if (instance == null)
                    {
                        Console.WriteLine("Using WAN IP: " + wanIp);
                        
                        instance = new VpnInstance(wanIp);
                        instance.start();
                        _instances.Add(instance);
                        return instance;
                    }

                    if (instance.isCooledDown())
                    {
                        Console.WriteLine("Reusing WAN IP: " + wanIp);
                        instance.start();
                        return instance;
                    }
                    
                    tryCount++;
                    Console.WriteLine("Burned IP -> get new one try: " + tryCount);
                }
                
                Console.WriteLine("Could not find fresh IP waiting 15 min");

                await Task.Delay(930000); // wait a little more than 15 min
            }
        }

        private static VpnInstance GetInstance(string wanIp)
        {
            return _instances.FirstOrDefault(instance => instance.getIpAddress().Equals(wanIp));
        }
        
        static async Task<string> WaitAndStartVpn()
        {
            StartVpn();

            while (!IsVpnRunning())
            {
                await Task.Delay(200);
            }
            
            await Task.Delay(17000);

            string wanIp = WanIpReader.GetWanIp();
            
            Console.WriteLine("Instance WAN IP: " + wanIp);

            return wanIp;
        }

        static void QuitVpn()
        {
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = _vpnPathExe,
                    Arguments = "--quit",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    WorkingDirectory = _vpnPath
                }
            };

            proc.Start();
        }

        static void StartVpn()
        {
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = _vpnPathExe,
                    Arguments = "--connect-shortcut=1636031168927 --remote-random",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    WorkingDirectory = _vpnPath
                }
            };

            Console.WriteLine("Starting VPN");
            proc.Start();
        }
    }
}