using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace PostcodeCheck.VpnUtil
{
    public static class WanIpReader
    {

        public static string GetWanIp()
        {
            string externalIpString = null;
            try
            {
                externalIpString = new WebClient().DownloadString("http://ipv4.icanhazip.com/").Replace("\\r\\n", "").Replace("\\n", "").Trim();
            }
            catch (Exception e)
            {
                Console.WriteLine("DNS fix");
                
                Thread.Sleep(5000);
                
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "/C ipconfig /flushdns";
                process.StartInfo = startInfo;
                process.Start();

                process.WaitForExit();

                System.Diagnostics.Process process2 = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo2 = new System.Diagnostics.ProcessStartInfo();
                startInfo2.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo2.FileName = "cmd.exe";
                startInfo2.Arguments = "/C ipconfig /registerdns";
                process2.StartInfo = startInfo2;
                process2.Start();

                process2.WaitForExit();
                
                System.Diagnostics.Process process3 = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo3 = new System.Diagnostics.ProcessStartInfo();
                startInfo3.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo3.FileName = "cmd.exe";
                startInfo3.Arguments = "/C ipconfig /release";
                process3.StartInfo = startInfo3;
                process3.Start();

                process3.WaitForExit();
                
                System.Diagnostics.Process process4 = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo4 = new System.Diagnostics.ProcessStartInfo();
                startInfo4.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo4.FileName = "cmd.exe";
                startInfo4.Arguments = "/C ipconfig /renew";
                process4.StartInfo = startInfo4;
                process4.Start();

                process4.WaitForExit();

                Thread.Sleep(5000);
                
                externalIpString = new WebClient().DownloadString("http://ipv4.icanhazip.com/").Replace("\\r\\n", "").Replace("\\n", "").Trim();
                
            }
                
            var externalIp = IPAddress.Parse(externalIpString);

            return externalIp.ToString();
        }

    }
}