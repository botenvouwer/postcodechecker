﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PostcodeCheck.DatabaseContext;
using PostcodeCheck.DomainModel;
using PostcodeCheck.SniffRunPlanStrategies.core;
using PostcodeCheck.SniffStrategies;
using PostcodeCheck.SniffStrategies.core;
using PostcodeCheck.VpnUtil;

namespace PostcodeCheck.SniffRunPlanStrategies
{
    public class VpnRunJob
    {
        protected PostcodeCheckerContext context;
        private SniffResult[] scanResults;
        private ISniffer sniffer;

        public int totalJobs => scanResults.Length;

        public ProxyData usedProxy { get; set; } = null;

        public int currentJob { get; private set; } = 0;

        public SniffResult lastSubject { get; private set; }

        public SniffResult currentSubject { get; private set; }

        public Task job { get; private set; }

        private int _queueDelay;
        public int queueDelay
        {
            get => _queueDelay;
            set => _queueDelay = 60000 * (value > 0 ? value : 0);
        }

        private int _queueSize = 1;
        public int queueSize
        {
            get => _queueSize;
            set => _queueSize = value > 0 ? value : 1;
        }

        public JobStatus JobStatus { get; private set; } = JobStatus.Off;

        public VpnRunJob(SniffResult[] scanResults, PostcodeCheckerContext context, ISniffer sniffer, int queueSize, int queueDelay)
        {
            this.scanResults = scanResults;
            this.context = context;
            this.sniffer = sniffer;
            this.queueDelay = queueDelay;
            this.queueSize = queueSize;
        }

        public VpnRunJob(SniffResult[] scanResults, PostcodeCheckerContext context, ISniffer sniffer)
        {
            this.scanResults = scanResults;
            this.context = context;
            this.sniffer = sniffer;
            this.queueDelay = 0;
            this.queueSize = 1;
        }

        public SniffResult sniff(SniffResult sniff)
        {
            getRawData(sniff);
            analyseRawData(sniff);

            return sniff;
        }

        public void getRawData(SniffResult sniff)
        {
            JobStatus = JobStatus.Scanning;
            if (sniffer.isNotScanned(sniff))
            {
                if (usedProxy != null)
                {
                    sniffer.getRawData(sniff, usedProxy);
                }
                else
                {
                    sniffer.getRawData(sniff);
                }
            }
        }

        public void analyseRawData(SniffResult sniff)
        {
            JobStatus = JobStatus.Analysing;
            if (sniffer.isNotAnalyzed(sniff))
            {
                sniffer.analyseRawData(sniff);
            }
        }

        private VpnInstance _instance;
        
        private async Task _run()
        {
            if (JobStatus != JobStatus.Off)
            {
                return;
            }

            JobStatus = JobStatus.Sniffing;
            
            _instance = await VpnUtilSwitcher.SwitchIpWithVpn();
            
            // int q = 100;
            int qsc = 0;
            for (int i = 0; i < scanResults.Length; i++)
            {
                //JobStatus = JobStatus.Sniffing;
                qsc++;
                currentJob++;
                lastSubject = currentSubject;
                currentSubject = scanResults[i];

                //Sniff uitvoeren

                try
                {
                    Console.WriteLine($"\t -> Sniffing {currentSubject.address}");
                    sniff(currentSubject);
                    Console.WriteLine($"\t -> Sniffing result: {currentSubject.code}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"\t -> Sniffing Failed due to Exception");
                    currentSubject.code = -6;
                    currentSubject.message = "Sniff mislukt: Er is iets misgegaan: foutcode " + (object)ex.HResult + " ~:~" + ex.Message;
                }

                context.Entry(currentSubject).State = EntityState.Modified;

                //Save changes to the DB every 100 cycles
                // q--;
                // if (q <= 0)
                // {
                //     q = 100;
                //     context.SaveChangesAsync();
                // }

                if (currentSubject.code == -4)
                {
                    _instance.end();
                    _instance = await VpnUtilSwitcher.SwitchIpWithVpn();
                }
                
                //Pause the scanning after [_queueDelay] time every [_queueSize] runs
                if (qsc >= 5)
                {
                    
                    JobStatus = JobStatus.Cooldown;
                    context.SaveChangesAsync();
                    
                    Console.WriteLine("\t-> Burned ip switching vpn");

                    _instance.end();
                    _instance = await VpnUtilSwitcher.SwitchIpWithVpn();

                    qsc = 0;
                    
                    // Thread.Sleep(_queueDelay);
                }
            }

            context.SaveChanges();
            JobStatus = JobStatus.Done;
        }

        public async Task Run()
        {
            _run().Wait();
        } 
    }
}