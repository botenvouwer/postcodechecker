﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PostcodeCheck.DomainModel;
using PostcodeCheck.SniffRunPlanStrategies.core;

namespace PostcodeCheck.SniffRunPlanStrategies
{
    public class VpnRunPlan : RunPlanCore
    {
        public VpnRunPlan(string scanName, string scanStrategy) : base(scanName, scanStrategy)
        {
        }

        public VpnRunPlan(string scanName, string scanStrategy, List<Address> addressList) : base(scanName, scanStrategy, addressList)
        {
        }

        public async Task runAsync()
        {
            await runAsync(0, 0);
        }

        public async Task runAsync(int queueSize, int queueDelay)
        {
            Status = JobStatusInitiation.Sniffing;
            // RunJobs = new List<RunJob>();

            if (needScanJobStack.Length > 0)
            {
                
                Console.WriteLine($"Running {needScanJobStack.Length} scans");
                VpnRunJob job = new VpnRunJob(needScanJobStack, context, sniffer, queueSize, queueDelay);

                // RunJobs.Add(job);

                await job.Run();
                
            }

            if (needAnalyseJobStack.Length > 0)
            {
                Console.WriteLine($"Running {needAnalyseJobStack.Length} analysis");
                VpnRunJob job = new VpnRunJob(needAnalyseJobStack, context, sniffer, 0, 0);
                // RunJobs.Add(job);

                await job.Run();
            }

            Status = JobStatusInitiation.Done;

        }

    }
}