﻿using System;
using System.Collections.Generic;
using System.Linq;
using PostcodeCheck.DomainModel;
using PostcodeCheck.SniffRunPlanStrategies.core;

namespace PostcodeCheck.SniffRunPlanStrategies
{
    public class LocalRunPlan : RunPlanCore
    {
        public LocalRunPlan(string scanName, string scanStrategy) : base(scanName, scanStrategy)
        {
        }

        public LocalRunPlan(string scanName, string scanStrategy, List<Address> addressList) : base(scanName, scanStrategy, addressList)
        {
        }

        public void runAsync()
        {
            runAsync(0, 0);
        }

        public void runAsync(int queueSize, int queueDelay)
        {
            Status = JobStatusInitiation.Sniffing;
            RunJobs = new List<RunJob>();

            if (needScanJobStack.Length > 0)
            {
                RunJob job = new RunJob(needScanJobStack, context, sniffer, queueSize, queueDelay);

                RunJobs.Add(job);

                job.run();
                
            }

            if (needAnalyseJobStack.Length > 0)
            {
                RunJob job = new RunJob(needAnalyseJobStack, context, sniffer, 0, 0);
                RunJobs.Add(job);

                job.run();
            }

            Status = JobStatusInitiation.Done;

        }

    }
}