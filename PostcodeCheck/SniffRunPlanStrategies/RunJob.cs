﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PostcodeCheck.DatabaseContext;
using PostcodeCheck.DomainModel;
using PostcodeCheck.SniffRunPlanStrategies.core;
using PostcodeCheck.SniffStrategies;
using PostcodeCheck.SniffStrategies.core;

namespace PostcodeCheck.SniffRunPlanStrategies
{
    public class RunJob
    {
        protected PostcodeCheckerContext context;
        private SniffResult[] scanResults;
        private ISniffer sniffer;

        public int totalJobs => scanResults.Length;

        public ProxyData usedProxy { get; set; } = null;

        public int currentJob { get; private set; } = 0;

        public SniffResult lastSubject { get; private set; }

        public SniffResult currentSubject { get; private set; }

        public Task job { get; private set; }

        private int _queueDelay;
        public int queueDelay
        {
            get => _queueDelay;
            set => _queueDelay = 60000 * (value > 0 ? value : 0);
        }

        private int _queueSize = 1;
        public int queueSize
        {
            get => _queueSize;
            set => _queueSize = value > 0 ? value : 1;
        }

        public JobStatus JobStatus { get; private set; } = JobStatus.Off;

        public RunJob(SniffResult[] scanResults, PostcodeCheckerContext context, ISniffer sniffer, int queueSize, int queueDelay)
        {
            this.scanResults = scanResults;
            this.context = context;
            this.sniffer = sniffer;
            this.queueDelay = queueDelay;
            this.queueSize = queueSize;
        }

        public RunJob(SniffResult[] scanResults, PostcodeCheckerContext context, ISniffer sniffer)
        {
            this.scanResults = scanResults;
            this.context = context;
            this.sniffer = sniffer;
            this.queueDelay = 0;
            this.queueSize = 1;
        }

        public SniffResult sniff(SniffResult sniff)
        {
            getRawData(sniff);
            analyseRawData(sniff);

            return sniff;
        }

        public void getRawData(SniffResult sniff)
        {
            JobStatus = JobStatus.Scanning;
            if (sniffer.isNotScanned(sniff))
            {
                if (usedProxy != null)
                {
                    sniffer.getRawData(sniff, usedProxy);
                }
                else
                {
                    sniffer.getRawData(sniff);
                }
            }
        }

        public void analyseRawData(SniffResult sniff)
        {
            JobStatus = JobStatus.Analysing;
            if (sniffer.isNotAnalyzed(sniff))
            {
                sniffer.analyseRawData(sniff);
            }
        }

        private void _run()
        {
            if (JobStatus != JobStatus.Off)
            {
                return;
            }

            JobStatus = JobStatus.Sniffing;

            int q = 100;
            int qsc = 0;
            for (int i = 0; i < scanResults.Length; i++)
            {
                //JobStatus = JobStatus.Sniffing;
                qsc++;
                currentJob++;
                lastSubject = currentSubject;
                currentSubject = scanResults[i];

                //Sniff uitvoeren

                try
                {
                    sniff(currentSubject);
                }
                catch (Exception ex)
                {
                    currentSubject.code = -6;
                    currentSubject.message = "Sniff mislukt: Er is iets misgegaan: foutcode " + (object)ex.HResult + " ~:~" + ex.Message;
                }

                context.Entry(currentSubject).State = EntityState.Modified;

                //Save changes to the DB every 100 cycles
                q--;
                if (q <= 0)
                {
                    q = 100;
                    context.SaveChangesAsync();
                }

                //Pause the scanning after [_queueDelay] time every [_queueSize] runs
                if (_queueSize != 1 && qsc >= _queueSize)
                {
                    JobStatus = JobStatus.Cooldown;
                    qsc = 0;
                    context.SaveChangesAsync();
                    Thread.Sleep(_queueDelay);
                }
            }

            context.SaveChangesAsync();
            JobStatus = JobStatus.Done;
        }

        public void run()
        {
            job = new Task(_run);
            job.Start();
        } 
    }
}