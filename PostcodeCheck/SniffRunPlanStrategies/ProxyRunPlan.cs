﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore.Query.ExpressionTranslators.Internal;
using PostcodeCheck.DomainModel;
using PostcodeCheck.SniffRunPlanStrategies.core;

namespace PostcodeCheck.SniffRunPlanStrategies
{
    public class ProxyRunPlan : RunPlanCore
    {
        public ProxyRunPlan(string scanName, string scanStrategy) : base(scanName, scanStrategy)
        {
        }

        public ProxyRunPlan(string scanName, string scanStrategy, List<Address> addressList) : base(scanName, scanStrategy, addressList)
        {
        }

        public void runAsyncWithAllProxies(int queueSize, int queueDelay)
        {
            Status = JobStatusInitiation.FetchProxies;
            List<ProxyData> proxyList = (from pd in context.Proxies where pd.inUse select pd).ToList();

            if (proxyList.Count == 0)
            {
                throw new ApplicationException("There are no proxy servers available in the database. Add proxies to the proxy table and check if the field inUse is true 'inUse = true'.");
            }

            runAsyncWithProxy(queueSize, queueDelay, proxyList);
        }

        public void runAsyncWithProxy(int queueSize, int queueDelay, List<ProxyData> proxyList)
        {
            Status = JobStatusInitiation.Sniffing;
            RunJobs = new List<RunJob>();

            //todo: Als dit zo is dan ook geen proxy servers ladden dus dit optillen naar hoger niveau en context naar lager niveau
            if (needScanJobStack.Length > 0)
            {
                int jobsPerProxy = totalNeedScanJobs / proxyList.Count;
                int leftoverJobs = totalNeedScanJobs % proxyList.Count;
                bool firstTime = true;
                bool onlyUseOne = (totalNeedScanJobs < proxyList.Count);
                
                int copyIndex = 0;
                foreach (var proxy in proxyList)
                {
                    int numberOfJobs = (onlyUseOne
                        ? totalNeedScanJobs
                        : (firstTime ? jobsPerProxy + leftoverJobs : jobsPerProxy));

                    SniffResult[] jobList = new SniffResult[numberOfJobs];

                    Array.Copy(needScanJobStack, copyIndex, jobList, 0, numberOfJobs);

                    RunJob job = new RunJob(jobList, context, sniffer, queueSize, queueDelay);
                    job.usedProxy = proxy;

                    RunJobs.Add(job);

                    job.run();

                    if (onlyUseOne)
                    {
                        break;
                    }

                    copyIndex += numberOfJobs;
                    firstTime = false;
                }
            }

            if (needAnalyseJobStack.Length > 0)
            {
                RunJob job = new RunJob(needAnalyseJobStack, context, sniffer, 0, 0);
                RunJobs.Add(job);

                job.run();
            }

            Status = JobStatusInitiation.Done;

        }
    }
}