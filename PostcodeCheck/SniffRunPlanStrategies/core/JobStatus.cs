﻿namespace PostcodeCheck.SniffRunPlanStrategies.core
{
    public enum JobStatus
    {
        Off,
        Scanning,
        Sniffing,
        Analysing,
        Cooldown,
        Done
    }

    public enum JobStatusInitiation
    {
        Start,
        LoadExisting,
        LoadNew,
        CheckMigrations,
        ConnectToDb,
        InitiateSniffer,
        BindingAddresses,
        Ready,
        Sniffing,
        Done,
        FetchProxies,
        SaveResults,
        SettingUpJobs
    }
}
