﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PostcodeCheck.DatabaseContext;
using PostcodeCheck.DomainModel;
using PostcodeCheck.FileSystem;
using PostcodeCheck.SniffStrategies.core;

namespace PostcodeCheck.SniffRunPlanStrategies.core
{
    public abstract class RunPlanCore
    {
        protected PostcodeCheckerContext context;
        protected Scan _scan;
        protected SniffResult[] scanResults;
        protected SniffResult[] needScanJobStack;
        protected SniffResult[] needAnalyseJobStack;
        public ISniffer sniffer;

        public Scan scan => _scan;
        public int totalScanResults => scanResults.Length;
        public int totalNeedScanJobs => needScanJobStack.Length;
        public int totalNeedAnalyseJobs => needAnalyseJobStack.Length;

        public List<RunJob> RunJobs { get; protected set; }

        public bool allDone
        {
            get
            {
                int count = 0;
                foreach (var run in RunJobs)
                {
                    if (run.job.IsCompleted)
                        count++;
                }

                return count == RunJobs.Count;
            }
        }

        public JobStatusInitiation Status { get; protected set; }
        public int AddressBindCount { get; private set; }

        public Task job { get; private set; }

        private void initiate(string scanName, string scanStrategy)
        {
            Status = JobStatusInitiation.Start;

            //Als er geen naam wordt opgegeven dan genereren we ereen
            if (String.IsNullOrEmpty(scanName))
            {
                scanName = Guid.NewGuid().ToString();
            }

            Status = JobStatusInitiation.ConnectToDb;
            context = new PostcodeCheckerContextFactory().CreateDbContext();
            context.Database.Migrate();

            _scan = (from s in context.Scans where s.scanName == scanName && s.scanStrategy == scanStrategy select s).FirstOrDefault();
        }

        protected void setSniffer()
        {
            //todo: Unsafe thows exception when scanStrategy does not exist -> use enum instead of string
            sniffer = SnifferFactory.getInstance((SnifferStrategy)Enum.Parse(typeof(SnifferStrategy), scanStrategy));
        }

        public void save()
        {
            if (Status == JobStatusInitiation.Done)
                context.SaveChanges();
        }

        private string scanName;
        private readonly string scanStrategy;
        private readonly List<Address> addressList;

        public RunPlanCore(string scanName, string scanStrategy)
        {
            this.scanName = scanName;
            this.scanStrategy = scanStrategy;
            setSniffer();

            job = new Task(_RunPlanCoreOld);
        }

        //constructor voor directe invoer van adressen
        public RunPlanCore(string scanName, string scanStrategy, List<Address> addressList)
        {
            this.scanName = scanName;
            this.scanStrategy = scanStrategy;
            this.addressList = addressList;
            setSniffer();

            job = new Task(_RunPlanCoreNew);
        }

        public void _RunPlanCoreOld()
        {
            initiate(scanName, scanStrategy);

            //todo: add option for when there is a brother strategy so we load those addresses instead of loading them from CSV
            //todo: add option for when we want to create a new RUN

            if (_scan != null)
            {
                //Scan already exists so we take what is already there
                Status = JobStatusInitiation.LoadExisting;
                scanResults = (from sr in context.SniffResults.Include("address") where sr.scanID == _scan.ID select sr).ToArray();

                setJobStack();
            }
            else
            {
                //todo: Move file access to higer level
                //Scan does not exist in DB so we create one first
                Status = JobStatusInitiation.LoadNew;
                List<Address> addressList = AddressListCSV.getAdressListFromCSV(scanName);
                prepFirstScan(scanName, scanStrategy, addressList);
            }

            Status = JobStatusInitiation.Ready;
        }

        //constructor voor directe invoer van adressen
        public void _RunPlanCoreNew()
        {
            initiate(scanName, scanStrategy);

            if (_scan != null)
            {
                scanName = scanName + Guid.NewGuid();
            }

            Status = JobStatusInitiation.LoadNew;
            prepFirstScan(scanName, scanStrategy, addressList);

            Status = JobStatusInitiation.Ready;
        }

        public void runInitialization()
        {
            job.Start();
        }

        private void prepFirstScan(string scanName, string scanStrategy, List<Address> addressList)
        {
            //Create new _scan
            _scan = new Scan() { scanName = scanName, scanStrategy = scanStrategy, scanDate = DateTime.Now };
            context.Scans.Add(_scan);

            //Create array for all the results
            scanResults = new SniffResult[addressList.Count];

            //Add the addresses to results
            AddressBindCount = 0;
            Status = JobStatusInitiation.BindingAddresses;
            foreach (var address in addressList)
            {
                SniffResult sniff = new SniffResult(address);
                sniff.scan = _scan;

                //todo: create better validation for address
                if (address.zipcode == "" || address.houseNumber == 0)
                {
                    sniff.message = "Postcode of adress nummer ontbreekt";
                    sniff.code = -1;
                }

                //Look if the address exists in DB when yes then we use the existing entity
                Address addressInDB = (from a in context.Addresses
                                       where
                                           a.zipcode == sniff.address.zipcode &&
                                           a.houseNumber == sniff.address.houseNumber &&
                                           a.addressAddition == sniff.address.addressAddition
                                       select a).FirstOrDefault();

                if (addressInDB != null)
                {
                    sniff.address = addressInDB;
                }

                //Note! the provId was first set inside the address field -> this was a mistake -> the provId should be attatched to a scan -> from now on we bind it to scan -> but we keep the external reference for the address intact in case we want to bind the address list with an external address database (like the Dutch BAG)
                sniff.externalKey = address.provId;

                scanResults[AddressBindCount] = sniff;
                AddressBindCount++;
            }

            Status = JobStatusInitiation.SaveResults;
            context.SniffResults.AddRange(scanResults);
            context.SaveChanges();

            setJobStack(true);

            Status = JobStatusInitiation.LoadNew;
        }

        protected void setJobStack()
        {
            setJobStack(false);
        }

        protected void setJobStack(bool isFirstScan)
        {
            Status = JobStatusInitiation.SettingUpJobs;

            if (isFirstScan)
            {
                this.needScanJobStack = scanResults;
                this.needAnalyseJobStack = new SniffResult[0];
                return;
            }

            List<SniffResult> needScan = new List<SniffResult>(); 
            List<SniffResult> needAnalyse = new List<SniffResult>(); 

            foreach (var job in scanResults)
            {
                if (sniffer.isNotScanned(job))
                {
                    needScan.Add(job);
                }
                else if (sniffer.isNotAnalyzed(job))
                {
                    needAnalyse.Add(job);
                }
            }

            this.needScanJobStack = needScan.ToArray();
            this.needAnalyseJobStack = needAnalyse.ToArray();
        }

    }
}
