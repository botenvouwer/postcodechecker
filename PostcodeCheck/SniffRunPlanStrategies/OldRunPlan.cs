﻿using System;
using Microsoft.EntityFrameworkCore;
using PostcodeCheck.DatabaseContext;
using PostcodeCheck.DomainModel;
using PostcodeCheck.SniffRunPlanStrategies.core;
using PostcodeCheck.SniffStrategies;

namespace PostcodeCheck.SniffRunPlanStrategies
{
    public class OldRunPlan : RunPlanCore
    {
        private int _currentJob = 0;
        public int currentJob => _currentJob;

        private bool _runInProgress = false;
        public bool runInProgress => _runInProgress;

        private SniffResult _lastSubject;
        public SniffResult lastSubject => _lastSubject;

        private SniffResult _currentSubject;
        public SniffResult currentSubject => _currentSubject;

        public OldRunPlan(string scanName, string scanStrategy) : base(scanName, scanStrategy)
        {
        }

        public SniffResult sniff(SniffResult sniff)
        {
            getRawData(sniff);
            analyseRawData(sniff);

            return sniff;
        }

        public void getRawData(SniffResult sniff)
        {
            if (sniffer.isNotScanned(sniff))
            {
                sniffer.getRawData(sniff);
            }
        }

        public void analyseRawData(SniffResult sniff)
        {
            if (sniffer.isNotAnalyzed(sniff))
            {
                sniffer.analyseRawData(sniff);
            }
        }

        public void run()
        {
            if (_runInProgress)
            {
                return;
            }

            _runInProgress = true;
            int q = 100;
            for (int i = 0; i < scanResults.Length; i++)
            {
                _currentJob++;
                _lastSubject = _currentSubject;
                _currentSubject = scanResults[i];
                sniff(_currentSubject);

                context.Entry(_currentSubject).State = EntityState.Modified;

                //Save changes to the DB every 100 cycles
                q--;
                if (q <= 0)
                {
                    q = 100;
                    context.SaveChanges();
                }
            }

            context.SaveChanges();
            _runInProgress = false;
            _currentJob = 0;
        }
    }
}
