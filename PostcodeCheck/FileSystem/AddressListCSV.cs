﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using PostcodeCheck.DomainModel;

namespace PostcodeCheck.FileSystem
{
    public class AddressListCSV
    {
        public static List<Address> getAdressListFromCSV(string scanName)
        {

            string url = string.Format(@"{0}\input\{1}.csv", Directory.GetCurrentDirectory(), scanName);

            if (File.Exists(url))
            {
                return getAdressListFromCSV((Stream)File.OpenRead(url));
            }
            else
            {
                throw new FileNotFoundException("Address file could not be located in: " + url);
            }
        }

        public static List<Address> getAdressListFromCSV(Stream file)
        {
            List<Address> addressList = new List<Address>();
            StreamReader streamReader = new StreamReader(file);
            int num = 0;
            while (!streamReader.EndOfStream)
            {
                ++num;
                string str = streamReader.ReadLine();
                if (num != 1)
                {
                    string[] strArray = str.Split(';');
                    try
                    {
                        addressList.Add(new Address()
                        {
                            provId = strArray[0],
                            zipcode = strArray[1],
                            houseNumber = int.Parse(strArray[2]),
                            addressAddition = strArray[3]
                        });
                    }
                    catch { }
                }
            }

            return addressList;
        }

        public static void createResult(string url, List<SniffResult> result)
        {
            if (result.Count == 0)
                return;
            using (StreamWriter streamWriter = new StreamWriter(url, false))
            {
                streamWriter.Write(formatCsvLine(result[0], true));
                foreach (SniffResult sniff in result)
                {
                    streamWriter.Write(formatCsvLine(sniff, false));
                }
            }
        }

        public static string formatCsvLine(SniffResult sniff, bool printHeader = false)
        {
            if (printHeader)
                return "id;zipcode;housenumber;addressAddition;availability;code;planCode;planDate;coperspeed;fiberspeed;upgradespeed;message\r\n";
            return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}\r\n", sniff.address.provId, sniff.address.zipcode, sniff.address.houseNumber, sniff.address.addressAddition, sniff.availability, sniff.code, sniff.planCode, sniff.planning, sniff.coperSpeed, sniff.fiberSpeed, sniff.upgradeSpeed, sniff.message);
        }
    }
}
