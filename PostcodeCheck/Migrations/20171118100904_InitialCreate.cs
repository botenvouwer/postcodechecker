﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PostcodeCheck.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "address",
                columns: table => new
                {
                    pk_address = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    address_addition = table.Column<string>(maxLength: 10, nullable: true),
                    house_number = table.Column<int>(nullable: false),
                    external_key = table.Column<string>(maxLength: 10, nullable: true),
                    zipcode = table.Column<string>(maxLength: 6, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_address", x => x.pk_address);
                });

            migrationBuilder.CreateTable(
                name: "proxy",
                columns: table => new
                {
                    pk_proxy = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ip = table.Column<string>(maxLength: 45, nullable: false),
                    pass = table.Column<string>(maxLength: 200, nullable: true),
                    port = table.Column<int>(nullable: false),
                    user = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_proxy", x => x.pk_proxy);
                });

            migrationBuilder.CreateTable(
                name: "scan",
                columns: table => new
                {
                    pk_scan = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    scan_date = table.Column<DateTime>(nullable: false),
                    scan_name = table.Column<string>(maxLength: 200, nullable: false),
                    scan_strategy = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_scan", x => x.pk_scan);
                });

            migrationBuilder.CreateTable(
                name: "scan_result",
                columns: table => new
                {
                    pk_scan_result = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    fk_address = table.Column<int>(nullable: false),
                    availability = table.Column<bool>(nullable: false),
                    code = table.Column<int>(nullable: false),
                    coper_speed = table.Column<int>(nullable: false),
                    fiber_speed = table.Column<int>(nullable: false),
                    message = table.Column<string>(maxLength: 600, nullable: true),
                    plan_code = table.Column<int>(nullable: false),
                    planning = table.Column<string>(maxLength: 200, nullable: true),
                    fk_proxy = table.Column<int>(nullable: true),
                    raw_result = table.Column<string>(nullable: true),
                    scan_date = table.Column<DateTime>(nullable: false),
                    fk_scan = table.Column<int>(nullable: false),
                    upgrade_speed = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_scan_result", x => x.pk_scan_result);
                    table.ForeignKey(
                        name: "FK_scan_result_address_fk_address",
                        column: x => x.fk_address,
                        principalTable: "address",
                        principalColumn: "pk_address",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_scan_result_proxy_fk_proxy",
                        column: x => x.fk_proxy,
                        principalTable: "proxy",
                        principalColumn: "pk_proxy",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_scan_result_scan_fk_scan",
                        column: x => x.fk_scan,
                        principalTable: "scan",
                        principalColumn: "pk_scan",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_scan_result_fk_address",
                table: "scan_result",
                column: "fk_address");

            migrationBuilder.CreateIndex(
                name: "IX_scan_result_fk_proxy",
                table: "scan_result",
                column: "fk_proxy");

            migrationBuilder.CreateIndex(
                name: "IX_scan_result_fk_scan",
                table: "scan_result",
                column: "fk_scan");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "scan_result");

            migrationBuilder.DropTable(
                name: "address");

            migrationBuilder.DropTable(
                name: "proxy");

            migrationBuilder.DropTable(
                name: "scan");
        }
    }
}
