﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json.Linq;
using PostcodeCheck.DomainModel;
using PostcodeCheck.SniffStrategies.core;

namespace PostcodeCheck.SniffStrategies
{
    internal class ZiggoSniffer : HttpSniffCore
    {
        public string baseUrl = "https://restapi.ziggo.nl";
        public string targetUrl = "1.0/availability/?zipcode={0}&housenumber={1}{2}";
        
        public override SniffResult getRawData(HttpClient httpClient, SniffResult sniff)
        {

            using (httpClient)
            {
                httpClient.BaseAddress = new Uri(baseUrl);
                Address address = sniff.address;
                string url = String.Format(targetUrl, address.zipcode, address.houseNumber, (!string.IsNullOrEmpty(address.addressAddition) ? "&extension=" + address.addressAddition : ""));

                try
                {
                    var result = httpClient.GetAsync(url).Result;

                    sniff.rawResult = result.Content.ReadAsStringAsync().Result;
                }
                catch (HttpRequestException e)
                {
                    sniff.code = -3;
                    sniff.message = e.Message;
                }
                catch (AggregateException e)
                {
                    sniff.code = -7;
                    sniff.message = e.Message;
                }
            }

            return sniff;
        }

        public override SniffResult analyseRawData(SniffResult sniffResult)
        {
            sniffResult.message = "Analyse niet uitgevoerd -> resultaat is anders dan verwacht";
            sniffResult.code = -2;

            //Eerst proberen of we het veld 'availability' kunnen vinden anders bestaad het niet en hebben we een andere json result binnen
            try
            {
                //Raw data omzetten naar dynamic object
                dynamic ziggoResponse = JsonConvert.DeserializeObject(sniffResult.rawResult);

                //Probeer het veld 'availability' te pakken
                //var x = ziggoResponse.availability;

                //todo: Betere methode bedenken om te controleren of variable beschikbaar is in dynamic object
                try
                {
                    if (ziggoResponse.description == "You tried to find something that isn't there")
                    {
                        sniffResult.availability = false;
                        sniffResult.code = -3;
                        sniffResult.message = ziggoResponse.message;

                        return sniffResult;
                    }
                }
                catch
                {
                }

                if (ziggoResponse.availability is JArray)
                {
                    var list = (JArray) ziggoResponse.availability;

                    if (list.Count > 0)
                    {
                        foreach (dynamic package in ziggoResponse.availability)
                        {
                            string name = (string) package.name;
                            bool available = (bool) package.available;

                            if (name == "catv" && available)
                            {
                                sniffResult.code = 4;
                                sniffResult.message = "Ziggo kabeltelevisie beschikbaar";
                            }

                            if (name == "internet" && available)
                            {
                                sniffResult.availability = true;
                                sniffResult.code = 2;
                                sniffResult.message = "Ziggo internet en digitaletelevisie beschikbaar";
                                break;
                            }

                            if (!available)
                            {
                                sniffResult.availability = false;
                                sniffResult.code = 0;
                                sniffResult.message = "Ziggo niet beschikbaar";
                            }
                        }
                    }
                    else
                    {
                        sniffResult.availability = false;
                        sniffResult.code = 0;
                        sniffResult.message = "Ziggo niet beschikbaar (availability leeg)";
                    }
                }
            }
            catch (RuntimeBinderException)
            {
                //Het is niet gelukt, het resultaat is anders dan vewacht sniff afblazen
                sniffResult.message = "JSON resultaat is anders dan verwacht";
                sniffResult.code = -2;
            }

            return sniffResult;
        }
        
    }
}
