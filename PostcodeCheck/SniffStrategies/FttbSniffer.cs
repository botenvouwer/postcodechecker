﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using PostcodeCheck.DomainModel;
using PostcodeCheck.SniffStrategies.core;

namespace PostcodeCheck.SniffStrategies
{
    class FttbSniffer : HttpSniffCore
    {
        public string targetUrl = "http://fttb.nl/check.php";

        public override SniffResult getRawData(HttpClient httpClient, SniffResult sniff)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.RequestUri = new Uri(targetUrl);
            request.Method = HttpMethod.Post;
            request.Headers.Add("x-requested-with", "XMLHttpRequest");
            request.Headers.Add("Origin", "http://fttb.nl");
            request.Headers.Add("Referer", "http://fttb.nl/");
            request.Headers.Add("Host", "fttb.nl");

            FormUrlEncodedContent urlEncodedContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("postcode", sniff.address.zipcode),
                new KeyValuePair<string, string>("huisnummer", string.Format("{0}{1}", sniff.address.houseNumber, sniff.address.addressAddition)),
                new KeyValuePair<string, string>("a", "a")
            });

            request.Content = urlEncodedContent;
            
            try
            {
                sniff.rawResult = httpClient.SendAsync(request).Result.Content.ReadAsStringAsync().Result;
            }
            catch (HttpRequestException e)
            {
                sniff.code = -3;
                sniff.message = e.Message;
            }
            catch (AggregateException e)
            {
                sniff.code = -7;
                sniff.message = e.Message;
            }

            return sniff;
        }

        public override SniffResult analyseRawData(SniffResult sniff)
        {
            //No analysis -> We only need the json in raw_result
            sniff.availability = false;
            sniff.code = 0;
            //throw new NotImplementedException();
            return sniff;
        }
    }
}
