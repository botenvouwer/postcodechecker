﻿using System;
using System.Net.Http;
using HtmlAgilityPack;
using PostcodeCheck.DomainModel;
using PostcodeCheck.SniffStrategies.core;

namespace PostcodeCheck.SniffStrategies
{
    internal class NetcoSniffer : HttpSniffCore
    {
        string netcoUrl = "https://netco-fpi-info.fourstack.nl";
        string requestUrl = "/addresses/search?address={0}+{1}+{2}";

        public override SniffResult getRawData(HttpClient httpClient, SniffResult sniff)
        {
            using (httpClient)
            {

                httpClient.BaseAddress = new Uri(netcoUrl);
                string requestUri = string.Format(requestUrl, sniff.address.zipcode, sniff.address.houseNumber, sniff.address.addressAddition);

                try
                {
                    sniff.rawResult = httpClient.GetAsync(requestUri).Result.Content.ReadAsStringAsync().Result;
                }
                catch (HttpRequestException e)
                {
                    sniff.code = -3;
                    sniff.message = e.Message;
                }
                catch (AggregateException e)
                {
                    sniff.code = -7;
                    sniff.message = e.Message;
                }
            }

            return sniff;
        }

        public override SniffResult analyseRawData(SniffResult sniff)
        {
            sniff.message = "Analyse niet uitgevoerd -> resultaat is anders dan verwacht";
            sniff.code = -2;

            if (sniff.rawResult == "Quota Exceeded")
            {
                sniff.code = -4;
                sniff.message = "Quota Exceeded";
                return sniff;
            }

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(sniff.rawResult);
            HtmlNode htmlNode = htmlDocument.DocumentNode.SelectSingleNode("/html/body/div[3]/div[2]/div/fieldset/dl/dt");
            sniff.availability = false;

            //Scan voor error pagina
            if (htmlNode != null && htmlNode.InnerText == "Error")
            {
                string innerText = htmlDocument.DocumentNode.SelectSingleNode("/html/body/div[3]/div[2]/div/fieldset/dl/dd").InnerText;
                sniff.message = innerText;
                sniff.code = -3;
                return (SniffResult)sniff;
            }

            HtmlNode htmlNode2 = htmlDocument.DocumentNode.SelectSingleNode("/html/body/div[3]/div[2]/div/fieldset/small/div/div/dl/dt");
            if (htmlNode2 != null)
            {
                string innerText = htmlDocument.DocumentNode.SelectSingleNode("/html/body/div[3]/div[2]/div/fieldset/small/div/div/dl/dd").InnerText;

                sniff.message = innerText;
                sniff.code = -3;
                return (SniffResult)sniff;
            }

            sniff.message = "";

            //Scan voor 1 - telefoonlijn
            HtmlNode htmlNode3 = htmlDocument.DocumentNode.SelectSingleNode("//*[@id=\"actual_data_copper_technology_types\"]/ dd[2]");
            if (htmlNode3 != null && htmlNode3.InnerText != "-")
            {
                sniff.coperSpeed = this.parseInt(htmlNode3.InnerText);
                sniff.message += string.Format("Coper: {0},", (object)htmlNode3.InnerText);
                sniff.code = 1;
                sniff.availability = true;
            }
            else
            {
                sniff.message += "Coper: none,";
            }

            //Scan voor 3 - glasvezel
            HtmlNode htmlNode4 = htmlDocument.DocumentNode.SelectSingleNode("//*[@id=\"actual_data_fiber_technology_types\"]/ dd[2]");
            if (htmlNode4 != null && htmlNode4.InnerText != "-")
            {
                sniff.fiberSpeed = this.parseInt(htmlNode4.InnerText);
                sniff.message += string.Format(" Fiber: {0},", (object)htmlNode4.InnerText);
                sniff.code = 3;
                sniff.availability = true;
            }
            else
            {
                sniff.message += " Fiber: none,";
            }

            if (!sniff.availability)
            {
                sniff.code = 0;
            }

            if (htmlDocument.DocumentNode.SelectSingleNode("/html/body/div[3]/div[3]/div/fieldset/dl/dt") == null)
            {
                HtmlNode htmlNode5 = htmlDocument.DocumentNode.SelectSingleNode("/html/body/div[3]/div[3]/div/fieldset/div/div/dl/dd[4]");
                HtmlNode htmlNode6 = htmlDocument.DocumentNode.SelectSingleNode("/html/body/div[3]/div[3]/div/fieldset/div/div/dl/dd[2]");

                string innerText3 = (htmlNode5 != null ? htmlNode5.InnerText : "-1");
                string innerText4 = (htmlNode6 != null ? htmlNode6.InnerText : "");

                sniff.upgradeSpeed = this.parseInt(innerText3);
                sniff.planning = innerText4;
                sniff.message += string.Format(" Upgrade: {0} -> {1},", (object)innerText3, (object)innerText4);
                sniff.planCode = 3;
            }
            return (SniffResult)sniff;
        }

        public int parseInt(string speed)
        {
            int value = 0;

            int.TryParse(speed.Split(' ')[0], out value);

            return value;
        }
    }
}
