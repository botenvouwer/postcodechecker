﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using HtmlAgilityPack;
using PostcodeCheck.DomainModel;
using PostcodeCheck.SniffStrategies.core;

namespace PostcodeCheck.SniffStrategies
{
    class ViberQSniffer : HttpSniffCore
    {
        /*
         * 15 min
         * 
         *  9842PH 64   Nog niet in aanmerking
         *  9884TC 8    Helaas, maar... [doorverwijzing naar website Snel Internet Groningen]
         *  9364PL 15   Gefeliciteerd, je kan je aansluiten bij ViberQ
         */
        
        string rekamUrl = "https://www.viberq.nl";
        string requestUrl = "";

        public override SniffResult getRawData(HttpClient httpClient, SniffResult sniff)
        {
            using (httpClient)
            {
                httpClient.BaseAddress = new Uri(rekamUrl);

                string page = httpClient.GetAsync("").Result.Content.ReadAsStringAsync().Result;
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(page);
                HtmlNode htmlNode = htmlDocument.DocumentNode.SelectSingleNode("//*[@id=\"zipcodechecker-nonce\"]");

                string nonce = htmlNode.GetAttributeValue("value", "");
                
                FormUrlEncodedContent urlEncodedContent = new FormUrlEncodedContent(new []
                {
                    new KeyValuePair<string, string>("zipcodechecker-zip", sniff.address.zipcode),
                    new KeyValuePair<string, string>("zipcodechecker-number", sniff.address.houseNumber + (!string.IsNullOrEmpty(sniff.address.addressAddition) ? " "+ sniff.address.addressAddition : "")),
                    new KeyValuePair<string, string>("zipcodechecker-nonce", nonce), //todo: get nonce from page
                    new KeyValuePair<string, string>("website-restriction", ""),
                    new KeyValuePair<string, string>("zipcodechecker-submit", "Ga verder")
                });

                try
                {
                    sniff.rawResult = httpClient.PostAsync(requestUrl, urlEncodedContent).Result.Content.ReadAsStringAsync().Result;
                }
                catch (HttpRequestException e)
                {
                    sniff.code = -3;
                    sniff.message = e.Message;
                }
                catch (AggregateException e)
                {
                    sniff.code = -7;
                    sniff.message = e.Message;
                }
            }

            return sniff;
        }

        public override SniffResult analyseRawData(SniffResult sniff)
        {
            
            if (sniff.rawResult.Contains("Nog niet in aanmerking"))
            {
                sniff.availability = false;
                sniff.code = 0;
            }
            else if (sniff.rawResult.Contains("Helaas, maar"))
            {
                sniff.availability = false;
                sniff.code = 2;
            }
            else if (sniff.rawResult.Contains("Gefeliciteerd!"))
            {
                sniff.availability = true;
                sniff.code = 1;
            }            
            else if (sniff.rawResult.Contains("Je hebt het formulier te vaak ingevuld"))
            {
                sniff.availability = false;
                sniff.code = -4;
            }
            else
            {
                sniff.availability = false;
                sniff.code = -2;
            }

            return sniff;
        }
    }
}
