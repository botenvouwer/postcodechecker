﻿namespace PostcodeCheck.SniffStrategies.core
{
    public class SnifferFactory
    {
        public static ISniffer getInstance(SnifferStrategy choice)
        {
            ISniffer sniffer = null;
            switch (choice)
            {
                case SnifferStrategy.ziggo:
                    sniffer = new ZiggoSniffer();
                    break;
                case SnifferStrategy.netco:
                    sniffer = new NetcoSniffer();
                    break;
                case SnifferStrategy.rekam:
                    sniffer = new RekamSniffer();
                    break;
                case SnifferStrategy.kabelnoord:
                    sniffer = new KabelNoordSniffer();
                    break;
                case SnifferStrategy.fttb:
                    sniffer = new FttbSniffer();
                    break;
                case SnifferStrategy.viberq:
                    sniffer = new ViberQSniffer();
                    break;
            }
            return sniffer;
        }
    }
}
