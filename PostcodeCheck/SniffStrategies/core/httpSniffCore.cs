﻿using System;
using System.Net;
using System.Net.Http;
using PostcodeCheck.DomainModel;

namespace PostcodeCheck.SniffStrategies.core
{
    public abstract class HttpSniffCore : ISniffer
    {
        public int[] isNotScannedCriteria { get; set; } = new[] { -7, -6, -5, -4 };
        public int[] isNotAnalyzedCriteria { get; set; } = new[] { -7, -6, -5, -4, -3, -2 };

        private HttpClient _client { get; set; }
        private HttpClientHandler _handler { get; set; }
        private ProxyData _proxy { get; set; }

        private HttpClient getClient(ProxyData p)
        {
            if (_client == null || !p.Equals(_proxy))
            {
                _proxy = p;
                _handler = new HttpClientHandler();
                _handler.Proxy = _proxy.getProxy();
                _handler.UseProxy = true;
                
                _client = new HttpClient(new LoggingHandler(_handler, p));
            }

            return _client;
        }

        public SniffResult getRawData(SniffResult sniff, ProxyData proxy)
        {
            sniff.scanDate = DateTime.Now;
            sniff.proxyID = proxy.ID;
            sniff.usedProxy = proxy;

            return getRawData(getClient(proxy), sniff);
        }

        public SniffResult getRawData(SniffResult sniff)
        {
            sniff.scanDate = DateTime.Now;

            HttpClient httpClient = new HttpClient(new LoggingHandler(new HttpClientHandler()));
            return getRawData(httpClient, sniff);
        }

        public abstract SniffResult getRawData(HttpClient httpClient, SniffResult sniff);

        public abstract SniffResult analyseRawData(SniffResult sniff);

        public bool isNotScanned(SniffResult sniff)
        {
            bool check = false;
            foreach (var isNotScannedCriterion in isNotScannedCriteria)
            {
                if (sniff.code == isNotScannedCriterion)
                    check = true;
            }

            return check;
        }

        public bool isNotAnalyzed(SniffResult sniff)
        {
            bool check = false;
            foreach (var isNotAnalyzedCriterion in isNotAnalyzedCriteria)
            {
                if (sniff.code == isNotAnalyzedCriterion)
                    check = true;
            }

            return check;
        }

        public bool isScannedAndAnalyzed(SniffResult sniff)
        {
            return (!isNotScanned(sniff) && !isNotAnalyzed(sniff));
        }
    }
}