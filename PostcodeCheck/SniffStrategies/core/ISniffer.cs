﻿using PostcodeCheck.DomainModel;

namespace PostcodeCheck.SniffStrategies.core
{
    public interface ISniffer
    {
        int[] isNotScannedCriteria { get; set; }
        int[] isNotAnalyzedCriteria { get; set; }

        SniffResult getRawData(SniffResult sniff);

        SniffResult getRawData(SniffResult sniff, ProxyData proxy);

        SniffResult analyseRawData(SniffResult sniff);

        bool isNotScanned(SniffResult sniff);

        bool isNotAnalyzed(SniffResult sniff);

        bool isScannedAndAnalyzed(SniffResult sniff);

    }
}