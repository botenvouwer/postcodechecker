﻿namespace PostcodeCheck.SniffStrategies.core
{
    public enum SnifferStrategy
    {
        netco,
        ziggo,
        rekam,
        kabelnoord,
        fttb,
        viberq
    }
}