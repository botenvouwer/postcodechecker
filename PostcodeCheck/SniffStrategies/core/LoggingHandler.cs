﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using PostcodeCheck.DomainModel;

namespace PostcodeCheck.SniffStrategies.core
{
    public class LoggingHandler : DelegatingHandler
    {
        private ProxyData usedProxy = null;

        public LoggingHandler(HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
        }

        public LoggingHandler(HttpMessageHandler innerHandler, ProxyData usedProxy)
            : base(innerHandler)
        {
            this.usedProxy = usedProxy;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpResponseMessage response = null;

            Directory.CreateDirectory("./logbook");

            var filename = "log_";

            if (usedProxy != null)
            {
                filename += usedProxy.ip + "_";
            }
            else
            {
                filename += "local_";
            }

            filename += DateTime.Now.ToString("dd-MM-yyyy") + ".txt";

            using (StreamWriter logbook = new StreamWriter("./logbook/" + filename, true)) { 

                logbook.WriteLine("Request:");
                logbook.WriteLine(request.ToString());
                if (request.Content != null)
                {
                    logbook.WriteLine(await request.Content.ReadAsStringAsync());
                }
                logbook.WriteLine();

                response = await base.SendAsync(request, cancellationToken);

                logbook.WriteLine("Response:");
                logbook.WriteLine(response.ToString());
                if (response.Content != null)
                {
                    logbook.WriteLine(await response.Content.ReadAsStringAsync());
                }
                logbook.WriteLine();
            }

            return response;
        }
    }
}