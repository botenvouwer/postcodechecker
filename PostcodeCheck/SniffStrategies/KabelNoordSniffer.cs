﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using HtmlAgilityPack;
using PostcodeCheck.DomainModel;
using PostcodeCheck.SniffStrategies.core;

namespace PostcodeCheck.SniffStrategies
{
    class KabelNoordSniffer : HttpSniffCore
    {
        string domainUrl = "https://www.glasvezelvankabelnoord.nl";
        string requestUrl = "/";

        public override SniffResult getRawData(HttpClient httpClient, SniffResult sniff)
        {
            using (httpClient)
            {

                httpClient.BaseAddress = new Uri(domainUrl);

                FormUrlEncodedContent urlEncodedContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("zip_code", sniff.address.zipcode),
                    new KeyValuePair<string, string>("house_number", sniff.address.houseNumber + (!string.IsNullOrEmpty(sniff.address.addressAddition) ? " "+ sniff.address.addressAddition : "")),
                    new KeyValuePair<string, string>("zip-code-submit", "zip-code-submit"),
                    new KeyValuePair<string, string>("nospam", ""),
                    new KeyValuePair<string, string>("email", ""),
                    new KeyValuePair<string, string>("status", "")
                });

                try
                {
                    var result = httpClient.PostAsync(requestUrl, urlEncodedContent).GetAwaiter().GetResult();
                    
                    sniff.rawResult = result.Content.ReadAsStringAsync().Result;
                }
                catch (HttpRequestException e)
                {
                    sniff.code = -3;
                    sniff.message = e.Message;
                }
                catch (AggregateException e)
                {
                    sniff.code = -7;
                    sniff.message = e.Message;
                }
            }

            return sniff;
        }

        public override SniffResult analyseRawData(SniffResult sniff)
        {
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(sniff.rawResult);

            HtmlNode htmlNode = htmlDocument.DocumentNode.SelectSingleNode("//*[@id=\"main\"]/div[1]/div[1]/div/h2");

            if (htmlNode != null)
            {
                if (htmlNode.InnerText.Contains("Postcode gevonden"))
                {
                    sniff.code = 1;
                    sniff.planCode = 3;
                    sniff.message = "Return url was https://www.glasvezelvankabelnoord.nl/postcode-gevonden";

                    return sniff;
                }

                if (htmlNode.InnerText.Contains("Postcode niet gevonden"))
                {
                    sniff.code = 0;
                    sniff.planCode = 0;
                    sniff.message = "Return url was https://www.glasvezelvankabelnoord.nl/postcode-niet-gevonden";

                    return sniff;
                }
            }

            sniff.message = "Onverwacht resultaat";
            sniff.code = -2;
            sniff.planCode = 0;

            return sniff;
        }
    }
}
