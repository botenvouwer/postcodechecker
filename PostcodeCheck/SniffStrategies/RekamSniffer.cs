﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using HtmlAgilityPack;
using PostcodeCheck.DomainModel;
using PostcodeCheck.SniffStrategies.core;

namespace PostcodeCheck.SniffStrategies
{
    class RekamSniffer : HttpSniffCore
    {
        string rekamUrl = "https://rekamverbindt.nl";
        string requestUrl = "/resultaat";

        public override SniffResult getRawData(HttpClient httpClient, SniffResult sniff)
        {
            using (httpClient)
            {

                httpClient.BaseAddress = new Uri(rekamUrl);
                //string requestUri = string.Format(requestUrl, sniff.address.zipcode, sniff.address.houseNumber, sniff.address.addressAddition);

                FormUrlEncodedContent urlEncodedContent = new FormUrlEncodedContent(new []
                {
                    new KeyValuePair<string, string>("postcode", sniff.address.zipcode),
                    new KeyValuePair<string, string>("huisnr", sniff.address.houseNumber.ToString()),
                    new KeyValuePair<string, string>("toev", sniff.address.addressAddition),
                    new KeyValuePair<string, string>("type", "p"),
                    new KeyValuePair<string, string>("hp", "")
                });

                try
                {
                    sniff.rawResult = httpClient.PostAsync(requestUrl, urlEncodedContent).Result.Content.ReadAsStringAsync().Result;
                }
                catch (HttpRequestException e)
                {
                    sniff.code = -3;
                    sniff.message = e.Message;
                }
                catch (AggregateException e)
                {
                    sniff.code = -7;
                    sniff.message = e.Message;
                }
            }

            return sniff;
        }

        public override SniffResult analyseRawData(SniffResult sniff)
        {

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(sniff.rawResult);

            //Glasvezel beschikbaar
            //*[@id="vanafhiersticky"]/div/div[1]/p
            //*[@id="vanafhiersticky"]/div/div[1]/p
            HtmlNode htmlNode = htmlDocument.DocumentNode.SelectSingleNode("//*[@id=\"vanafhiersticky\"]/div/div[1]/p");
            if (htmlNode != null)
            {
                if (htmlNode.InnerText.Contains("U heeft een glasvezelaansluiting"))
                {
                    sniff.availability = true;
                    sniff.code = 3;
                    
                }
                else if (htmlNode.InnerText.Contains("U heeft een kabelaansluiting"))
                {
                    sniff.availability = true;
                    sniff.code = 2;
                }

                sniff.message = htmlNode.InnerText;

                return sniff;
                //todo: betere analyse maken en snelheden meenemen in resultaten
            }

            //Geen verbinding beschikbaar
            //*[@id="vanafhiersticky"]/div/p[1]
            HtmlNode htmlNode2 = htmlDocument.DocumentNode.SelectSingleNode("//*[@id=\"vanafhiersticky\"]/div/p[1]");
            if (htmlNode2 != null && htmlNode2.InnerText.Contains("niet bekend"))
            {
                sniff.availability = false;
                sniff.code = 0;
                sniff.message = htmlNode2.InnerText;

                return sniff;
            }

            sniff.code = -2;
            return sniff;
        }
    }
}
