﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace PostcodeCheck.DatabaseContext
{
    public class PostcodeCheckerContextFactory : IDesignTimeDbContextFactory<PostcodeCheckerContext>
    {
        public static string connectionString { get; set; } = "Host=localhost;Database=PostcodeCheck;Username=test;Password=test";

        public PostcodeCheckerContext CreateDbContext()
        {
            return CreateDbContext(new string[] { });
        }

        public PostcodeCheckerContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PostcodeCheckerContext>();
            builder.UseNpgsql(connectionString);

            return new PostcodeCheckerContext(builder.Options);
        }
    }
}
