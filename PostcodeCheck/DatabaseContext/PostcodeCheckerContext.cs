﻿using Microsoft.EntityFrameworkCore;
using PostcodeCheck.DomainModel;

namespace PostcodeCheck.DatabaseContext
{
    public class PostcodeCheckerContext : DbContext
    {
        public DbSet<Scan> Scans { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<ProxyData> Proxies { get; set; }
        public DbSet<SniffResult> SniffResults { get; set; }

        public PostcodeCheckerContext(DbContextOptions<PostcodeCheckerContext> options) : base(options)
        {

        }


    }
}