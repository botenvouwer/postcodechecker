﻿# PostcodeChecker 3.3 #

He vader! dit is de readme voor je programma.

### Hoe te gebruiken ###

* Alle code voorbeelden staan gewrapt in ". Als je de code copy paste copy dan alleen de inhoud tussen de " en niet de " zelf! Alles tussen [] zijn parameters die nog ingevuld moeten worden. Als je copy paste kijk dan goed wat er op de plek van de [] moet komen te staan.

1. In de console ga je naar de directory waar "PostcodeCheckConsole.exe" staat;
2. Roep PostcodeCheckConsole.exe met de volgende 4 parameters -> "PostcodeCheckConsole.exe [String scanName] [String scanType] [*Int grooteBatch] [*Int wachttijdPerBatchInMin]" (zie ook kopje Scan strategieën/ types);
3. Wacht tot de scan voltooid is, mocht er iets foutgaan check dan eerst de input CSV;
4. Is de scan klaar kijk dan wat het unieke scan id is (pk_scan): SELECT * FROM scan;
5. Als de scan klaar is kun je met deze query snel kijken of het resultaat goed is: "SELECT code, COUNT(code) FROM debug WHERE pk_scan = [pk_scan] GROUP BY code ORDER BY code;";
6. Mochten er veel - codes in staan ga dan eerst na wat er precies is foutgegaan. Is er bijvoorbeeld een -2 dan moet de analyse worden aangepast is er een -6 dan moet de code gedebugged worden;
7. Is het resultaat goed kijk deze dan nog even door met steek proeven om te valideren dat het resultaat betrouwbaar is (Het kan zijn dat de analyse faalt als een doelwit de HTML of JSON aanpast).

Tip! Kijk ook naar de standaard views die zijn gemaakt. Deze joinen standaart al veel tabellen bij elkaar.

### Betekenis code veld ###
!Let op! Het kan zijn dat sommige codes niet kloppen met de werkelijkehid omdat de analyse niet goed is ingesteld. In zulke gevallen kan de analyse worden aangepast en opnieuw worden uitgevoerd.

-7	- AggregateException							Kan voorkomen geen idee waarom dit gebeurt
-6	- uncaught exception							Tijdens het scannen van dit adres is een foutmelding opgetreden die nog niet goed wordt afgevangen.
-5	- Adres is nog niet gescand
-4	- Blokkeer pagina								In het geval van deze code moet het adres opnieuw gescand worden.
-3	- Error pagina									
-2	- Onverwacht resultaat							In het geval van deze code moet het ruwe resultaat worden bestudeerd en waarschijnlijk moet de scan worden aangepast. Als dat het geval is moet het adres opnieuw gescand worden.
-1	- Adres gegevens foutief
0 	- Internet verbinding is niet beschikbaar		
1	- Teleffonlijn is beschikbaar					
2	- Coax is beschikbaar							
3	- Glasvezel is beschikbaar		
4	- Coax alleen voor tv beschikbaar

### Betekenis plan code ###

0	- Geen plan informatie
1	- Teleffonlijn niet beschikbaar maar komt wel beschikbaar					
2	- Coax niet beschikbaar maar komt wel beschikbaar								
3	- Glasvezel niet beschikbaar maar komt wel beschikbaar				

### code concept ###
Onderstaand is een concept voor de status codes. Door gebruik te maken van oneven getallen voor eenzijdige situaties kunnen we de optelling daarvan gebruiken voor de situaties waarin meerdere mogelijkheden waar zijn.

3 	- a
5 	- b
7 	- c

8 	- a,b
10	- a,c
12	- b,c
15	- a, b, c

### Scan strategieën/ types ###
[scanName] netco 5000 142 	-> scand voor kpn gegevens van "netco-fpi-info.fourstack.nl". Laat de scan om de 5000 scans 142 minuten wachten. Per proxy worden er 500 addressen gescand en we hebben 10 proxy servers. Dus 5000 addressen kunnen er per keer gescand worden. Na de eerste analyse is een tweede waarschijnlijk noodzakelijk omdat de quota toch nog exceeded is. Als we een paar runs verzamelen kunnen we gaan kijken waarom en wanneer dit optreed.
[scanName] ziggo			-> scand ziggo api. Zeer betrouwbare bron zonder limitaties.
[scanName] rekam			-> scand https://rekamverbindt.nl
[scanName] kabelnoord		-> scand https://www.glasvezelvankabelnoord.nl
[scanName] fttb				-> scand http://fttb.nl/check.php

# Database toegang #
User:admin
Pass:lopo34*h#